# predictprotein #
### A docker image for the [predictprotein](https://www.predictprotein.org) ([RostLab](http://www.rostlab.org/)) pipeline. ###

PredictProtein started out by predicting secondary structure and returning families of related proteins. Solvent accessibility and transmembrane helix prediction followed suit shortly thereafter. Over the two decades that PredictProtein has been operating, we have substantially expanded the breadth of structural annotations, e.g. by adding predictions of non-regular secondary structure and intrinsically disordered regions, disulphide bridges and inter-residue contacts, and finally by also covering trans-membrane beta barrels structures. We have also added important resources for the prediction of protein function, e.g. assisting in the annotation of subcellular localization (LocTree, LocTree2, NLSpred), identifying protein-protein interaction sites (ISIS) and protein-DNA binding sites (DISIS & SomeNA to be released shortly). We have added a few simple tools to predict enzymatic activity (unfortunately, this method did not survive the move from New York to Munich) and to infer GeneOntology numbers through sequence homology (MetaStudent). Another major addition are the tools that predict the effect of amino acid changes upon protein function (SNAP & SNAP2), and directly upon protein structure (in preparation).

## Pre-Requirements ##

Docker

**Dependencies**

* PredictProtein Database ([http://www.rostlab.org/services/ppmi/download_file?format=gzip&file_to_download=db](http://www.rostlab.org/services/ppmi/download_file?format=gzip&file_to_download=db))

## Usage ##

** Docker **

The PredictProtein docker image can be run in various ways:

Interactive:
```
$ docker run --rm -it -v <LOCAL_OUTPUT_DIRECTORY>:/mnt/local-storage bromberglab/predictprotein predictprotein [OPTIONS]
```

Capsulated:
```
$ docker run --rm -v <LOCAL_OUTPUT_DIRECTORY>:/mnt/local-storage bromberglab/predictprotein predictprotein [OPTIONS]
```

To mount a downloaded database add the following argument:
```
-v <LOCAL_DATA_DIRECTORY>:usr/share/rostlab-data/data/
```

To download the latest release of the database run:
```
$ docker run -t -d --name=my_predictprotein -v <LOCAL_OUTPUT_DIRECTORY>:/mnt/local-storage bromberglab/predictprotein
$ docker exec my_predictprotein setupdb
```

Then either attach to the running container to run *predictprotein* via an interactive session
```
$ docker exec -i -t my_predictprotein /bin/bash
```
or execute *predictprotein* directly
```
$ docker exec my_predictprotein predictprotein [OPTIONS]

```

Finally stop the container with
```
$ docker stop my_predictprotein

```

** Example **

Print the PredictProtein help:
```
$ docker run --rm bromberglab/predictprotein predictprotein --help
```

Run the PredictProtein providing a fasta input file:
```
$ docker run --rm -v <LOCAL_OUTPUT_DIRECTORY>:/mnt/local-storage bromberglab/predictprotein predictprotein -o /mnt/local-storage/test_out/ --seqfile /mnt/local-storage/query.fasta
```
The file *query.fasta* has to be located in the directory <LOCAL_OUTPUT_DIRECTORY> mounted into the running container via the -v flag)

** Notes: **

Using the --rm argument, docker will delete the container once exited.

## License ##

This project is licensed under [Academic Software License Agreement](https://rostlab.org/owiki/index.php/Academic_Software_License_Agreement).

## Citation ##

Please refer to [https://www.predictprotein.org](https://www.predictprotein.org).

## About ##

PredictProtein is developed and maintained by the [RostLab](http://www.rostlab.org/). The docker image is created and maintained by Maximilian Miller ([BrombergLab](http://www.bromberglab.org/)). Feel free to contact us for support (<services@bromberglab.org>).
