#!/usr/bin/env bash

##-------------------------------------------------------
# SETUP SCRIPT
##-------------------------------------------------------

# set UTF-8 environment
echo 'LC_ALL=en_US.UTF-8' >> /etc/environment
echo 'LANG=en_US.UTF-8' >> /etc/environment
echo 'LC_CTYPE=en_US.UTF-8' >> /etc/environment

# setup perlbrew
export PERLBREW_ROOT=/usr/share/profphd/prof/perl5
export PERLBREW_HOME=/tmp/.perlbrew
perlbrew init
source /usr/share/profphd/prof/perl5/etc/bashrc
perlbrew install -j `nproc` --notest --noman perl-5.10.1
ln -s /usr/share/profphd/prof/perl5/perls/perl-5.10.1/bin/perl /usr/bin/perl5.10.1
# set environment variables for library accordingly
export PERL5LIB=/usr/share/perl5/

# install consurf
(echo y;echo o conf prerequisites_policy follow;echo o conf commit)|cpan
cpan install Bio::Perl Config::IniFiles List::Util
cd /opt/
git clone https://github.com/Rostlab/ConSurf.git
cd ./ConSurf/3rd\ party\ software/
# install cd-hit via apt-get, version here is wrong and inconsistent with use in ConSurf
#./rate4site_build.sh
#mv /opt/ConSurf/3rd\ party\ software/rate4site.3.2.source_slow/sourceMar09/rate4site /usr/bin/rate4site_doublerep
#mv /opt/ConSurf/3rd\ party\ software/rate4site.3.2.source_fast/sourceMar09/rate4site /usr/bin/rate4site
./cd-hit_build.sh
mv /opt/ConSurf/3rd\ party\ software/cdhit/cd-hit /usr/bin/cd-hit
cd /opt/ConSurf/
aclocal
automake --force-missing --add-missing
autoconf
./configure
make
make install
# copy libs to correct location
/usr/bin/install -c -m 644 CONSURF_FUNCTIONS.pm CONSURF_CONSTANTS.pm MSA_parser.pm pdbParser.pm prepareMSA.pm TREE_parser.pm cp_rasmol_gradesPE_and_pipe.pm consurfrc.default consurf_new.py chimera_consurf.cmd '/usr/share/perl5/'
# replace consurf config
cp /project/sources/consurfrc.default /usr/local/share/consurf/consurfrc.default
# in /usr/local/bin/consurf: remove defined in lines 1247, 1255 and 1267
cp /project/sources/consurf /usr/bin/consurf
# remove installed version
rm /usr/local/bin/consurf

# replace edited files
# Can't use a hash as a reference at /usr/share/perl5/GO/IO/Dotty.pm line 104. Replaced by: URL=>$opts{'base_url'}.$term->acc,
cp /project/sources/Dotty.pm /usr/share/perl5/GO/IO/Dotty.pm

# replace predictprotein config and make files
cp /project/sources/predictproteinrc.default /usr/share/predictprotein/predictproteinrc.default
cp /project/sources/MakefilePP.mk /usr/share/predictprotein/MakefilePP.mk

# change perl version for specific scripts
perl_legacy="\#\!\/usr\/bin\/perl5.10.1 -w"
sed -i "1s/.*/$perl_legacy/" /usr/share/profphd/prof/prof.pl
sed -i "1s/.*/$perl_legacy/" /usr/share/profphd/prof/embl/scr/lib-col.pl
sed -i "1s/.*/$perl_legacy/" /usr/share/profphd/prof/embl/phd.pl
sed -i "1s/.*/$perl_legacy/" /usr/share/librg-utils-perl/copf.pl
sed -i "1s/.*/$perl_legacy/" /usr/share/librg-utils-perl/hssp_filter.pl
sed -i "1s/.*/$perl_legacy/" /usr/share/profphd/prof/scr/conv_prof.pl

# add blosum62 matrix for psic 
mkdir /usr/share/psic
cp /project/sources/blosum62_psic.txt /usr/share/psic/blosum62_psic.txt
# add psic binary
mv /project/sources/psic /usr/bin/psic
chmod 755 /usr/bin/psic

# add database setup
cp /project/sources/setupdb.sh /usr/local/bin/setupdb
chmod 755 /usr/local/bin/setupdb
