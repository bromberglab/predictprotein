#!/bin/bash

if [ -d /usr/share/rostlab-data/ ]; then
        echo "Removing current database..."
        rm -rf /usr/share/rostlab-data/
fi

mkdir /usr/share/rostlab-data/
cd /usr/share/rostlab-data/

echo "Downloading database release..."
wget -O rostlab-data.txz "http://www.rostlab.org/services/ppmi/download_file?format=gzip&file_to_download=db"

echo "Extracting..."
xz -d rostlab-data.txz
tar xvf rostlab-data.tar
rm -f rostlab-data.tar

echo "Done."